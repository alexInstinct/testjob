am4core.useTheme(am4themes_animated);

var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
chart.data = generateChartData();

$( "#selectMetricNames" ).change(function() {
    chart.data = generateChartData();
});

// Create axes
var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.minGridDistance = 50;


var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
var series = chart.series.push(new am4charts.LineSeries());
series.dataFields.valueY = "metric_value";
series.dataFields.dateX = "timestamp";
series.strokeWidth = 2;
series.minBulletDistance = 10;
series.tooltipText = "{valueY}";
series.tooltip.pointerOrientation = "vertical";
series.tooltip.background.cornerRadius = 20;
series.tooltip.background.fillOpacity = 0.5;
series.tooltip.label.padding(12,12,12,12)

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = dateAxis;
chart.cursor.snapToSeries = series;


function generateChartData() {
    var name = $( "#selectMetricNames option:selected" ).text();;
    var dataArr = [];
    $.ajax({
        url: "/api/metrics/"+name,
        dataType: 'json',
        async: false,
        success: function(json){
            dataArr = json;
        }
    });

    dataArr.forEach( function (v) {
        v.timestamp = new Date(v.timestamp);
    });

    return dataArr;
}