package com.rhonda.entity.db;

import com.google.gson.annotations.SerializedName;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Document(collection = "timeseries")
public class TimeSeries {

    @Id
    private String id;
    private Date timestamp;
    @Field("metric_name")
    @SerializedName("metric_name")
    private String metricName;
    @Field("metric_value")
    @SerializedName("metric_value")
    private String metricValue;

    public TimeSeries(String id, Date timestamp, String metricName, String metricValue) {
        this.id = id;
        this.timestamp = timestamp;
        this.metricName = metricName;
        this.metricValue = metricValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public String getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(String metricValue) {
        this.metricValue = metricValue;
    }

    @Override
    public String toString() {
        return "TimeSeries{" +
                "id='" + id + '\'' +
                ", timestamp=" + timestamp +
                ", metricName='" + metricName + '\'' +
                ", metricValue='" + metricValue + '\'' +
                '}';
    }
}
