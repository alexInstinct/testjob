package com.rhonda.config;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

@Configuration
@ComponentScan(basePackages = {"com.rhonda.service", "com.rhonda.dao"})
public class SpringConfig extends AbstractMongoConfiguration {

    @Override
    @Bean
    public MongoClient mongoClient() {
        return new MongoClient("127.0.0.1");
    }

    @Override
    public String getDatabaseName() {
        return "local";
    }
}
