package com.rhonda.dao;

import com.rhonda.entity.db.TimeSeries;

import java.util.List;

public interface TimeSeriesDao {
    List<TimeSeries> listAll();
    void add(TimeSeries timeSeries);
    List<String> getMetricNameList();
    List<TimeSeries> getByMetricName(String metricName);
}
