package com.rhonda.dao;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.client.MongoCollection;
import com.rhonda.entity.db.TimeSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TimeSeriesDaoImpl implements TimeSeriesDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<TimeSeries> listAll() {
        return mongoTemplate.findAll(TimeSeries.class, "timeseries");
    }

    @Override
    public void add(TimeSeries timeSeries) {
        mongoTemplate.insert(timeSeries);
    }

    @Override
    public List<String> getMetricNameList() {

        List<String> metricsList = new ArrayList<>();

        Query query = new Query();
        query.addCriteria(new Criteria().where("metric_name"));
        for (String s : mongoTemplate.getCollection("timeseries").distinct("metric_name", String.class))
            metricsList.add(s);
        return metricsList;
    }

    @Override
    public List<TimeSeries> getByMetricName(String metricName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("metric_name").is(metricName));
        List<TimeSeries> timeSeries = mongoTemplate.find(query, TimeSeries.class);
        return timeSeries;
    }
}
