package com.rhonda.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rhonda.entity.db.TimeSeries;
import com.rhonda.service.TimeSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class ApiController {

    @Autowired
    TimeSeriesService timeSeriesService;

    @PostMapping("/send")
    public String sendData(HttpEntity<String> httpEntity){

        String json = httpEntity.getBody();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        TimeSeries metric = gson.fromJson(json, TimeSeries.class);

        timeSeriesService.add(metric);

        return "Success"; //BAD
    }

    @GetMapping("/metric_names")
    public String metricNamesList() {
        List<String> metricNamesList = new ArrayList<>();
        metricNamesList.addAll(timeSeriesService.getMetricNameList());
        return new Gson().toJson(metricNamesList);
    }

    @GetMapping("/metrics/{name}")
    public String metrics(@PathVariable("name") String metricsName){
        List<TimeSeries> metricsList = new ArrayList<>();
        metricsList.addAll(timeSeriesService.getByMetricName(metricsName));
        return new Gson().toJson(metricsList);
    }

}
