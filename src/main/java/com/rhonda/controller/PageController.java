package com.rhonda.controller;

import com.rhonda.service.TimeSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class PageController {

    @Autowired
    public TimeSeriesService timeSeriesService;

    @GetMapping("/")
    public String timeSeries(Model model) {
        model.addAttribute("metricNames", timeSeriesService.getMetricNameList());
        return "index";
    }

}
