package com.rhonda.service;

import com.rhonda.entity.db.TimeSeries;

import java.util.List;

public interface TimeSeriesService {
    List<TimeSeries> listAll();
    void add(TimeSeries timeSeries);
    List<TimeSeries> getByMetricName(String metricName);
    List<String> getMetricNameList();
}
