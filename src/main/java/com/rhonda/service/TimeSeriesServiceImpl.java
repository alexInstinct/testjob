package com.rhonda.service;

import com.rhonda.dao.TimeSeriesDao;
import com.rhonda.entity.db.TimeSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimeSeriesServiceImpl implements TimeSeriesService {

    @Autowired
    TimeSeriesDao timeSeriesDao;

    @Override
    public List<TimeSeries> listAll() {
        return timeSeriesDao.listAll();
    }

    @Override
    public void add(TimeSeries timeSeries) {
        timeSeriesDao.add(timeSeries);
    }

    @Override
    public List<TimeSeries> getByMetricName(String metricName) {
        return timeSeriesDao.getByMetricName(metricName);
    }

    @Override
    public List<String> getMetricNameList() {
        return timeSeriesDao.getMetricNameList();
    }


}
